<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoDocumento extends Model
{
    use HasFactory;

    protected $table = 'tipodocumento';

    /**
     * Atributos que son asignables de manera masiva.
     *
     * @var string[]
     */
    protected $fillable = [
        'id',
        'tipo'
    ];

    /**
     * Los atributos que deben estar ocultos en la serializacion.
     *
     * @var array
     */
    protected $hidden = [
    ];


}
