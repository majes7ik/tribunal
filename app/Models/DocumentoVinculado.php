<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentoVinculado extends Model
{
    use HasFactory;

    protected $table = 'documento_vinculado';

    /**
     * Atributos que son asignables de manera masiva.
     *
     * @var string[]
     */
    protected $fillable = [
        'id',
        'iddocumento',
        'vinculado_a',
    ];

    /**
     * Los atributos que deben estar ocultos en la serializacion.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
