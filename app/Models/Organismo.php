<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class Organismo extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'organismo';

    /**
     * Atributos que son asignables de manera masiva.
     *
     * @var string[]
     */
    protected $fillable = [
        'id',
        'nombre',
        'externo'
    ];

    /**
     * Los atributos que deben estar ocultos en la serializacion.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function documentos(){
        return $this->hasMany(Documento::class,'idorganismo');
    }
}
