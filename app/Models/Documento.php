<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;


class Documento extends Model
{
    use HasApiTokens, HasFactory, Notifiable;


    protected $table = 'documento';

    /**
     * Atributos que son asignables de manera masiva.
     *
     * @var string[]
     */

    protected $fillable = [
        'id',
        'idtipo',
        'numero',
        'año',
        'idorganismo',
        'fecha_inicio',
        'tema',
        'informacion',
        'caratula',
        'cantidad_fojas',
        'esta_vinculado',
    ];

    /**
     * Los atributos que deben estar ocultos en la serializacion.
     *
     * @var array
     */
    protected $hidden = [
    ];


    // Se mapean lo que seria los objetos con las relaciones de la base de datos.

    public function organismo(){
        return $this->belongsTo(Organismo::class,'idorganismo');
    }

    public function tipoDocumento(){
        return $this->belongsTo(TipoDocumento::class,'idtipo');
    }

    public function observaciones(){
        return $this->hasMany(Observacion::class, 'iddocumento');
    }

    public function documentosVinculados(){
        return $this->belongsToMany(Documento::class,'documento_vinculado','vinculado_a','iddocumento')
            ->with('tipoDocumento')
            ->with('organismo');
    }

}
