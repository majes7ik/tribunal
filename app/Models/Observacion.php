<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class Observacion extends Model
{
    use HasApiTokens, HasFactory, Notifiable;


    protected $table = 'observacion';

    /**
     * Atributos que son asignables de manera masiva.
     *
     * @var string[]
     */
    protected $fillable = [
        'id',
        'iddoumento',
        'informacion'
    ];


    /**
     * Los atributos que deben estar ocultos en la serializacion.
     *
     * @var array
     */
    protected $hidden = [
    ];

    // Se mapean lo que seria los objetos con las relaciones de la base de datos.

    public function documento(){
        return $this->belongsTo(Documento::class);
    }
}
