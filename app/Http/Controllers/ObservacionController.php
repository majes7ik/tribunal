<?php

namespace App\Http\Controllers;

use App\Models\Observacion;
use Illuminate\Http\Request;

class ObservacionController extends Controller
{


   // Guarda una observacion y la asocia a un documento.
    public function store(Request $request)
    {
        $observacion = new Observacion();
        $observacion->iddocumento = $request->iddocumento;
        $observacion->informacion = $request->informacion;

        $res = $observacion->save();

        if ($res) {
            return response()->json(['message' => "Observacion creada con exito y asociada al documento con id $request->iddocumento"], 201);
        }
        return response()->json(['message' => 'Error al crear una observacion'], 500);

    }



    // Elimina la observacion solicitada
    // $id -> id de la observacion que se quiere eliminar
    public function destroy($id)
    {
        $res = Observacion::destroy($id);

        if ($res) {
            return response()->json(['message' => "Observacion  con id $id borrada con exito"], 201);
        }
        return response()->json(['message' => 'Error al borrar la observacion'], 500);
    }
}
