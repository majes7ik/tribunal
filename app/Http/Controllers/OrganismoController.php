<?php

namespace App\Http\Controllers;

use App\Models\Organismo;


class OrganismoController extends Controller
{

    // Muestra todos los organismos cargados en la base
    public function index()
    {
        return Organismo::get();
    }


    // Devuelve un organismo pasado por parametro y sus documentos
    // Siempre y cuando el organismo sea externo.
    public function show($id)
    {
        $organismo = Organismo::where('id', $id)
            ->with('documentos')
            ->get();

        if ($organismo[0]->externo == 1) {
            return $organismo;
        }
        else {
            return response()->json(['message' => 'El organismo consultado no es externo'], 201);
        }

    }

}
