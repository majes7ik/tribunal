<?php

namespace App\Http\Controllers;

use App\Models\Documento;
use App\Models\DocumentoVinculado;
use App\Models\TipoDocumento;
use Illuminate\Http\Request;
use App\Http\Controllers\date;


class DocumentoController extends Controller
{


    // Muestra todos los documentos cargados en la base
    public function index()
    {
        return Documento::with('observaciones')
            ->with('organismo')
            ->with('tipoDocumento')
            ->get();
    }


    // Muestra todos los documentos asociados a un expediente
    // $id -> es el id del expediente que se quiere consultar.
    public function verificarDocExpediente($id){

        return Documento::where('id',$id)
            ->with('documentosVinculados')
            ->with('organismo')
            ->get();
    }


    // Guarda un documento en la base de datos
    public function store(Request $request)
    {
        $documento = new Documento();
        $documento->idtipo = $request->idtipo;
        $documento->numero = $request->numero;
        $documento->año = now();
        $documento->idorganismo = $request->idorganismo;
        $documento->fecha_inicio = now();
        $documento->tema = $request->tema;
        $documento->informacion = $request->informacion;
        $documento-> caratula = $request->caratula;
        $documento->cantidad_fojas = $request->cantidad_fojas;


        $res = $documento->save();

        if ($res) {
            return response()->json(['message' => 'Documento creado con exito'], 201);
        }
        return response()->json(['message' => 'Error al crear el documento'], 500);
    }


    // Vincula un documento a un expediente.
    // Como parametro de la peticion se debe enviar $id_documento y $id_expediente.
    public function vincularDocumentoExpediente(Request $request){

        $documento = Documento::where('id',$request->id_expediente)->get();
        $tipoDocumento = TipoDocumento::find($documento[0]->idtipo);
        $documentoVinculado = new DocumentoVinculado();
        $documentoVinculado->iddocumento = $request->id_documento;
        $documentoVinculado->vinculado_a = $request->id_expediente;


        if ($tipoDocumento->tipo == 'Expediente'){
            $res = $documentoVinculado->save();
        } else {
            return response()->json(['message' => 'ERROR: Solo se le puede vincular documentos a un expediente'], 500);
        }


        if ($res) {
            return response()->json(['message' => 'El documento se vinculo correctamente al expediente.'], 201);
        }
        return response()->json(['message' => 'Error al vincular documento al expediente.'], 500);

    }



    // ELimina un documento de la base de datos
    // $id -> id del documento que se quiere eliminar.
    public function destroy($id)
    {
        $res = Documento::destroy($id);

        if ($res) {
            return response()->json(['message' => "Documento con id $id borrado con exito"], 201);
        }
        return response()->json(['message' => 'Error al borrar el documento'], 500);
    }
}
