<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DocumentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('documento')->insert([
            'id' => 1,
            'idtipo' => '1',
            'numero' => 2540,
            'año' => Carbon::now()->format('Y-m-d'),
            'idorganismo' => '1',
            'fecha_inicio' => Carbon::now()->format('Y-m-d H:i:s'),
            'tema' => 'un tema de prueba 1',
            'informacion' => 'una informacion de prueba 1',
            'caratula' => 'una caratula prueba 1',
            'cantidad_fojas' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);


        DB::table('documento')->insert([
            'id' => 2,
            'idtipo' => '1',
            'numero' => 2400,
            'año' => Carbon::now()->format('Y-m-d'),
            'idorganismo' => '1',
            'fecha_inicio' => Carbon::now()->format('Y-m-d H:i:s'),
            'tema' => 'un tema de prueba 2',
            'informacion' => 'una informacion de prueba 2',
            'caratula' => 'una caratula prueba 2',
            'cantidad_fojas' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('documento')->insert([
            'id' => 3,
            'idtipo' => '2',
            'numero' => 2401,
            'año' => Carbon::now()->format('Y-m-d'),
            'idorganismo' => '1',
            'fecha_inicio' => Carbon::now()->format('Y-m-d H:i:s'),
            'tema' => 'un tema de prueba 3',
            'informacion' => 'una informacion de prueba 3',
            'caratula' => 'una caratula prueba 3',
            'cantidad_fojas' => 20,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('documento')->insert([
            'id' => 4,
            'idtipo' => '2',
            'numero' => 2402,
            'año' => Carbon::now()->format('Y-m-d'),
            'idorganismo' => '2',
            'fecha_inicio' => Carbon::now()->format('Y-m-d H:i:s'),
            'tema' => 'un tema de prueba 4',
            'informacion' => 'una informacion de prueba 4',
            'caratula' => 'una caratula prueba 4',
            'cantidad_fojas' => 25,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('documento')->insert([
            'id' => 5,
            'idtipo' => '3',
            'numero' => 2403,
            'año' => Carbon::now()->format('Y-m-d'),
            'idorganismo' => '2',
            'fecha_inicio' => Carbon::now()->format('Y-m-d H:i:s'),
            'tema' => 'un tema de prueba 5',
            'informacion' => 'una informacion de prueba 5',
            'caratula' => 'una caratula prueba 5',
            'cantidad_fojas' => 50,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('documento')->insert([
            'id' => 6,
            'idtipo' => '3',
            'numero' => 2404,
            'año' => Carbon::now()->format('Y-m-d'),
            'idorganismo' => '3',
            'fecha_inicio' => Carbon::now()->format('Y-m-d H:i:s'),
            'tema' => 'un tema de prueba 6',
            'informacion' => 'una informacion de prueba 6',
            'caratula' => 'una caratula prueba 6',
            'cantidad_fojas' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('documento')->insert([
            'id' => 7,
            'idtipo' => '3',
            'numero' => 2405,
            'año' => Carbon::now()->format('Y-m-d'),
            'idorganismo' => '4',
            'fecha_inicio' => Carbon::now()->format('Y-m-d H:i:s'),
            'tema' => 'un tema de prueba 7',
            'informacion' => 'una informacion de prueba 7',
            'caratula' => 'una caratula prueba 7',
            'cantidad_fojas' => 10,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
