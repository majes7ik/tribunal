<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ObservacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('observacion')->insert([
            'id' => 1,
            'iddocumento' => '3',
            'informacion' => 'esta es una observacion de ejemplo para el documento con id numero 3',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('observacion')->insert([
            'id' => 2,
            'iddocumento' => '5',
            'informacion' => 'esta es una observacion de ejemplo para el documento con id numero 5',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
