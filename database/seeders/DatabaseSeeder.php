<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    // Se hace llamada a los seeder para insertar datos de prueba en la base.

    public function run()
    {
        $this->call(TipoDocumentoSeeder::class);
        $this->call(OrganismoSeeder::class);
        $this->call(DocumentoSeeder::class);
        $this->call(DocumentoVinculado::class);
        $this->call(ObservacionSeeder::class);
    }
}
