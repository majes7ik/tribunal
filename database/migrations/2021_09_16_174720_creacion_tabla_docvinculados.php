<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreacionTablaDocvinculados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documento_vinculado', function (Blueprint $table) {
            $table->id();
            $table->foreignId('iddocumento');
            $table->foreign('iddocumento')->references('id')->on('documento');
            $table->foreignId('vinculado_a');
            $table->foreign('vinculado_a')->references('id')->on('documento');
            $table->unique(['iddocumento', 'vinculado_a']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documento_vinculado');
    }
}
