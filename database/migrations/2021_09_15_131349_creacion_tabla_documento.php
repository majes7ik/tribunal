<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreacionTablaDocumento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documento', function (Blueprint $table) {
            $table->id();
            $table->foreignId('idtipo');
            $table->foreign('idtipo')->references('id')->on('tipodocumento');
            $table->integer('numero');
            $table->date('año');
            $table->foreignId('idorganismo');
            $table->foreign('idorganismo')->references('id')->on('organismo');
            $table->date('fecha_inicio');
            $table->string('tema');
            $table->string('informacion');
            $table->string('caratula')->nullable(true);
            $table->integer('cantidad_fojas')->nullable(true);
            //$table->foreignId('esta_vinculado')->nullable(true);
            //$table->foreign('esta_vinculado')->references('id')->on('documento');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documento');
    }
}
