<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DocumentoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('documentos', App\Http\Controllers\DocumentoController::class)->middleware('api');
Route::apiResource('observaciones', App\Http\Controllers\ObservacionController::class)->middleware('api');
Route::apiResource('organismos', App\Http\Controllers\OrganismoController::class)->middleware('api');

Route::get('verificardocexpediente/{id}', [DocumentoController::class,'verificarDocExpediente']);


Route::post('vincularDocumentoExpediente', [DocumentoController::class,'vincularDocumentoExpediente']);


